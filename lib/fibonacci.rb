class Fibonacci
  def create_series(previous_number, new_number, limit)
    while new_number < limit
      previous_number, new_number = yield(previous_number, new_number)
      puts previous_number
    end
  end
end

