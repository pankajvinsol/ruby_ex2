require_relative "../lib/fibonacci.rb"

start_number = 0
next_number = 1
series_limit = 1000

fibonacci = Fibonacci.new
fibonacci.create_series(start_number, next_number, series_limit) do |previous_number, new_number|
  previous_number, new_number = new_number, previous_number + new_number
end

